package net.dankito.billing.model

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


abstract class InvoiceUnit(val localeForCurrenciesAndDates: Locale = Locale.getDefault()) {

    protected open fun formatCurrency(value: Double, locale: Locale = localeForCurrenciesAndDates): String {
        return NumberFormat.getCurrencyInstance(locale).format(value)
    }

    protected open fun formatDecimalHoursString(decimalHours: Double, countDecimalPlaces: Int = 2): String {
        return String.format("%.${countDecimalPlaces}f", decimalHours)
    }

    protected open fun formatDate(date: LocalDate, dateFormat: DateTimeFormatter): String {
        return date.format(dateFormat)
    }

    protected open fun round(value: Double, countDecimalPlaces: Int): Double {
        var decimal = BigDecimal(value)

        decimal = decimal.setScale(countDecimalPlaces, RoundingMode.HALF_UP)

        return decimal.toDouble()
    }

}