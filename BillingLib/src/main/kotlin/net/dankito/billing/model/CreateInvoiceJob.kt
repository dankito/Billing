package net.dankito.billing.model

import java.io.File

open class CreateInvoiceJob(val invoice: Invoice,
                            val templateFile: File,
                            val pdfOutputFile: File? = null)