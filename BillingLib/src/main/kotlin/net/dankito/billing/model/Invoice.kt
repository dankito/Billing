package net.dankito.billing.model

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

open class Invoice(val items: List<InvoiceItem>,
              val invoicingDate: LocalDate = LocalDate.now(),
              val invoiceNumber: String = calculateInvoiceNumber(invoicingDate),
              val valueAddedTaxRate: Double = 0.19,
              val client: Client? = null,
              val invoiceStartDate: LocalDate = calculateInvoiceStartDate(invoicingDate),
              val invoiceEndDate: LocalDate = calculateInvoiceEndDate(invoicingDate),
              localeForCurrenciesAndDates: Locale = Locale.getDefault())
    : InvoiceUnit(localeForCurrenciesAndDates) {

    companion object {

        val InvoicingDateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy")

        val InvoiceStartDateFormat = DateTimeFormatter.ofPattern("dd. MMMM")

        val InvoiceEndDateFormat = DateTimeFormatter.ofPattern("dd. MMMM yyyy")

        val InvoiceNumberFromInvoicingDateFormat = DateTimeFormatter.ofPattern("yyMMdd'1'")


        fun calculateInvoiceStartDate(invoicingDate: LocalDate): LocalDate {
            if (isAtEndOfMonth(invoicingDate)) {
                return invoicingDate.withDayOfMonth(1)
            }
            else {
                return invoicingDate.minusMonths(1).withDayOfMonth(1)
            }
        }

        fun calculateInvoiceEndDate(invoicingDate: LocalDate): LocalDate {
            if (isAtEndOfMonth(invoicingDate)) {
                return invoicingDate
            }
            else {
                val previousMonth = invoicingDate.minusMonths(1)

                return previousMonth.withDayOfMonth(previousMonth.lengthOfMonth())
            }
        }

        private fun isAtEndOfMonth(date: LocalDate): Boolean {
            return date.dayOfMonth >= date.lengthOfMonth() - 4
        }

        fun calculateInvoiceNumber(invoicingDate: LocalDate): String {
            return invoicingDate.format(InvoiceNumberFromInvoicingDateFormat)
        }

    }


    open val netAmount: Double
        get() = items.sumByDouble { it.netAmount }

    open val netAmountString: String
        get() = formatCurrency(netAmount)

    open val valueAddedTax: Double
        get() = Math.floor(netAmount * valueAddedTaxRate)

    open val valueAddedTaxString: String
        get() = formatCurrency(valueAddedTax)

    open val totalAmount: Double
        get() = netAmount + valueAddedTax

    open val totalAmountString: String
        get() = formatCurrency(totalAmount)


    open val invoicingDateString: String
        get() = formatDate(invoicingDate, InvoicingDateFormat)

    open val invoiceStartDateString: String
        get() = formatDate(invoiceStartDate, InvoiceStartDateFormat)

    open val invoiceEndDateString: String
        get() = formatDate(invoiceEndDate, InvoiceEndDateFormat)

}