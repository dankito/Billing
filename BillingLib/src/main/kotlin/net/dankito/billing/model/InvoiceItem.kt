package net.dankito.billing.model


open class InvoiceItem(val hourlyWage: Double,
                       val countHours: Double,
                       val description: String? = null,
                       var index: Int = 1)
    : InvoiceUnit() {

    // countHours may not be exact, e.g. 146.80972222222223 instead of as displayed by Harvest 146.81, which makes a
    // few Cents difference in final net amount -> round countHours to seconds decimal place
    open val netAmount: Double
        get() = hourlyWage * round(countHours, 2)


    open val hourlyWageString: String
        get() = formatCurrency(hourlyWage)

    open val countHoursString: String
        get() = formatDecimalHoursString(countHours, 2)

    open val netAmountString: String
        get() = formatCurrency(netAmount)

}