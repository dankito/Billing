package net.dankito.billing.model

class Address(var streetName: String, var streetNumber: String, var postalCode: String, var city: String)