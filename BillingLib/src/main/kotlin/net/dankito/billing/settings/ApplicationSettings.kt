package net.dankito.billing.settings

import net.dankito.billing.model.Address
import net.dankito.billing.model.Client
import net.dankito.billing.timetracker.model.Credentials


class ApplicationSettings(var timeTrackerDataImporterUserName: String = "",
                          var timeTrackerDataImporterPassword: String = "",
                          var lastSelectedInvoiceTemplateFilePath: String = "",
                          var lastSelectedInvoiceOutputFilePath: String = "",
                          var invoiceItemHourlyWage: Double = 0.0,
                          var invoiceItemDescription: String = "",
                          var valueAddedTax: Double = 0.19,
                          var lastSelectedClient: Client = Client("", Address("", "", "", ""))) {


    val areTimeTrackerDataImporterCredentialsSet: Boolean
        get() = timeTrackerDataImporterUserName.isNotBlank() && timeTrackerDataImporterPassword.isNotBlank()

    fun getTimeTrackerDataImporterCredentials(): Credentials {
        return Credentials(timeTrackerDataImporterUserName, timeTrackerDataImporterPassword)
    }

}