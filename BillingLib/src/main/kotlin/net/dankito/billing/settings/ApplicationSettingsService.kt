package net.dankito.billing.settings

import com.google.gson.GsonBuilder
import org.slf4j.LoggerFactory
import java.io.*


open class ApplicationSettingsService {

    companion object {
        private val log = LoggerFactory.getLogger(ApplicationSettingsService::class.java)
    }


    var settings: ApplicationSettings
        protected set


    init {
        settings = readApplicationSettings()
    }


    fun settingsUpdated() {
        saveApplicationSettings()
    }


    protected open fun readApplicationSettings(): ApplicationSettings {
        val applicationSettingsFile = getApplicationSettingsFile()

        if (applicationSettingsFile.exists()) {
            try {
                val reader = BufferedReader(FileReader(applicationSettingsFile))

                return GsonBuilder().create().fromJson(reader, ApplicationSettings::class.java)
            } catch (e: Exception) {
                log.error("Could not read and deserialize ApplicationSettings", e)
            }
        }

        return ApplicationSettings()
    }

    protected open fun saveApplicationSettings() {
        val applicationSettingsFile = getApplicationSettingsFile()

        try {
            val writer = BufferedWriter(FileWriter(applicationSettingsFile))

            GsonBuilder().setPrettyPrinting().create().toJson(settings, writer)

            writer.close()
        } catch (e: Exception) {
            log.error("Could not serialize and write ApplicationSettings to file", e)
        }
    }

    protected open fun getApplicationSettingsFile() = File("settings.json")

}