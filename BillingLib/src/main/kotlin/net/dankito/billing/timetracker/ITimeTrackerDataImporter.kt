package net.dankito.billing.timetracker

import net.dankito.billing.timetracker.model.Credentials
import net.dankito.billing.timetracker.model.TrackedTimes
import kotlin.concurrent.thread


interface ITimeTrackerDataImporter {

    fun retrieveTrackedTimesAsync(credentials: Credentials, callback: (trackedTimes: TrackedTimes) -> Unit) {
        thread {
            callback(retrieveTrackedTimes(credentials))
        }
    }

    fun retrieveTrackedTimes(credentials: Credentials): TrackedTimes

}