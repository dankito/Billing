package net.dankito.billing.timetracker.model


open class Task(val name: String, timeEntries: MutableList<TimeEntry> = ArrayList())
    : TimeEntriesContainer(timeEntries) {

    override fun toString(): String {
        return name
    }

}