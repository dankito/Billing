package net.dankito.billing.timetracker.model


open class TrackedTimes(val entries: List<TimeEntry>, val days: List<TrackedDay>, val months: List<TrackedMonth>,
                        val projects: List<Project>, val task: List<Task>)