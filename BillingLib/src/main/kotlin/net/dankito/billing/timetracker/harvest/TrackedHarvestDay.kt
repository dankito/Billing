package net.dankito.billing.timetracker.harvest

import net.dankito.billing.timetracker.model.TimeEntry
import net.dankito.billing.timetracker.model.TrackedDay
import java.time.LocalDate


class TrackedHarvestDay(date: LocalDate, entries: List<TimeEntry>)
    : TrackedDay(date, entries) {

    /**
     * Displayed tracked time for a day in Harvest WebUI dependents on rounded minutes for each time entry
     */
    override val totalTrackedMinutesRounded: Int
        get() = entries.sumBy { it.totalTrackedMinutesRounded }

}