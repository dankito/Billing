package net.dankito.billing.timetracker

import net.dankito.billing.timetracker.harvest.TrackedHarvestDay
import net.dankito.billing.timetracker.model.TimeEntry
import net.dankito.billing.timetracker.model.TrackedDay
import net.dankito.billing.timetracker.model.TrackedMonth
import java.time.LocalDate


class TimeEntriesGrouper {

    fun groupByDays(entries: List<TimeEntry>): List<TrackedDay> {
        return entries
                .groupBy( { it.date }, { it } )
                .map { TrackedHarvestDay(it.key, it.value) }
    }

    fun groupByMonths(days: List<TrackedDay>): List<TrackedMonth> {
        return days
                .groupBy( { LocalDate.of(it.date.year, it.date.monthValue, 1) }, { it } )
                .map { TrackedMonth(it.key, it.value) }
    }

}