package net.dankito.billing.timetracker.model


class Credentials(val userName: String, val password: String)