package net.dankito.billing

import fr.opensagres.xdocreport.converter.ConverterTypeTo
import fr.opensagres.xdocreport.converter.Options
import fr.opensagres.xdocreport.core.document.DocumentKind
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry
import fr.opensagres.xdocreport.template.TemplateEngineKind
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata
import net.dankito.billing.model.CreateInvoiceJob
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


class InvoiceCreator {

    fun create(job: CreateInvoiceJob) {
        val pdfOutputFile = job.pdfOutputFile ?: createDefaultPdfOutputFile(job)

        createPdfFile(job, pdfOutputFile)
    }

    private fun createDefaultPdfOutputFile(job: CreateInvoiceJob): File {
        return File(job.templateFile.parentFile, "%04d.%02d.%02d ${job.invoice.client?.name}.pdf"
                .format(job.invoice.invoicingDate.year, job.invoice.invoicingDate.monthValue,
                        job.invoice.invoicingDate.dayOfMonth))
    }

    fun createPdfFile(job: CreateInvoiceJob, pdfOutputFile: File) {
        val report = XDocReportRegistry.getRegistry().loadReport(FileInputStream(job.templateFile),
                TemplateEngineKind.Velocity)

        val context = report.createContext()

        context.put("invoice", job.invoice)

        job.invoice.items.forEachIndexed { index, invoiceItem -> invoiceItem.index = index + 1 } // one based

        // this is XDocReporter's logic to show items. The template here is a row in a table
        val metadata = FieldsMetadata()
        metadata.addFieldAsList("invoiceItem.index")
        metadata.addFieldAsList("invoiceItem.description")
        metadata.addFieldAsList("invoiceItem.hourlyWageString")
        metadata.addFieldAsList("invoiceItem.countHoursString")
        metadata.addFieldAsList("invoiceItem.netAmountString")
        report.fieldsMetadata = metadata
        context.put("invoiceItem", job.invoice.items)

        val pdfOutputStream = FileOutputStream(pdfOutputFile)

        val options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF)

        report.convert(context, options, pdfOutputStream)

        pdfOutputStream.close()
    }

}