package net.dankito.billing.timetracker.harvest

import net.dankito.billing.timetracker.model.Credentials
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class HarvestTimeTrackerDataImporterTest {

    private val underTest = HarvestTimeTrackerDataImporter()


    @Test
    fun retrieveTrackedTimes() {
        val result = underTest.retrieveTrackedTimes(Credentials("", "")) // set your Harvest accountId and token here

        assertThat(result.months).isNotEmpty()

        assertThat(result.months).extracting("month").doesNotHaveDuplicates()
        assertThat(result.months).extracting("trackedTimeInSeconds").doesNotContain(0.0)

        assertThat(result.days).isNotEmpty()
        assertThat(result.days).extracting("date").doesNotHaveDuplicates()
        assertThat(result.days).extracting("trackedTimeInSeconds").doesNotContain(0.0)

        assertThat(result.entries).isNotEmpty()
        assertThat(result.entries).extracting("trackedTimeInSeconds").doesNotContain(0.0)

        assertThat(result.projects).isNotEmpty()
        assertThat(result.projects).extracting("name", String::class.java).isNotEmpty()
        assertThat(result.projects).extracting("trackedTimeEntries").isNotEmpty()
        assertThat(result.projects).extracting("trackedDays").isNotEmpty()
        assertThat(result.projects).extracting("trackedMonths").isNotEmpty()

        assertThat(result.task).isNotEmpty()
        assertThat(result.task).extracting("name", String::class.java).isNotEmpty()
        assertThat(result.task).extracting("trackedTimeEntries").isNotEmpty()
        assertThat(result.task).extracting("trackedDays").isNotEmpty()
        assertThat(result.task).extracting("trackedMonths").isNotEmpty()
    }

}