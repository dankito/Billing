package net.dankito.billing

import net.dankito.billing.model.*
import org.junit.Test
import java.io.File


class InvoiceCreatorTest {

    companion object {
        private const val TemplateFileName = "InvoiceTemplate.odt"

        // don't know why, works when testing with Gradle command line but not in IntelliJ
        private val templateFileUrl = InvoiceCreatorTest::class.java.classLoader.getResource(TemplateFileName)
        private val TemplateFile = File(templateFileUrl.toURI())
    }

    private val underTest = InvoiceCreator()


    @Test
    fun create() {
        underTest.create(CreateInvoiceJob(
                Invoice(listOf(InvoiceItem(75.0, 111.1, "Danke an die beste Frau der Welt")),
                        client = Client("Marieke Musterfrau",
                                Address("Musterstraße", "42", "12345", "Musterstadt"))),
                TemplateFile, null
        ))
    }

}