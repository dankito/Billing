package net.dankito.billing.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate


class InvoiceTest {

    @Test
    fun calculateInvoiceStartDate_StartOfMonth() {
        val result = Invoice.calculateInvoiceStartDate(LocalDate.of(2018, 8, 3))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 1))
    }

    @Test
    fun calculateInvoiceStartDate_EndOfMonth() {
        val result = Invoice.calculateInvoiceStartDate(LocalDate.of(2018, 7, 31))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 1))
    }

    @Test
    fun calculateInvoiceStartDate_AlmostEndOfMonth() {
        val result = Invoice.calculateInvoiceStartDate(LocalDate.of(2018, 7, 29))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 1))
    }

    @Test
    fun calculateInvoiceEndDate_StartOfMonth() {
        val result = Invoice.calculateInvoiceEndDate(LocalDate.of(2018, 8, 3))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 31))
    }

    @Test
    fun calculateInvoiceEndDate_EndOfMonth() {
        val result = Invoice.calculateInvoiceEndDate(LocalDate.of(2018, 7, 31))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 31))
    }

    @Test
    fun calculateInvoiceEndDate_AlmostEndOfMonth() {
        val result = Invoice.calculateInvoiceEndDate(LocalDate.of(2018, 7, 29))

        assertThat(result).isEqualTo(LocalDate.of(2018, 7, 29))
    }

    @Test
    fun calculateInvoiceNumber() {
        val result = Invoice.calculateInvoiceNumber(LocalDate.of(2018, 7, 31))

        assertThat(result).isEqualTo("1807311")
    }

}