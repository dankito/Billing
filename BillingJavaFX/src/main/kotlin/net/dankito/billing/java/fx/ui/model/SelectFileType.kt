package net.dankito.billing.java.fx.ui.model


enum class SelectFileType {

    SelectFile,
    SelectDirectory,
    SaveFile

}