package net.dankito.billing.java.fx.ui.window.main.controls

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import net.dankito.billing.java.fx.res.Icons
import net.dankito.billing.java.fx.ui.extensions.setBorder
import net.dankito.billing.settings.ApplicationSettingsService
import tornadofx.*


class TimeTrackerDataImporterSettingsView(private val refreshTimeTrackerDataListener: (() -> Unit)? = null) : View() {

    companion object {
        private const val TextFieldHeight = 26.0
    }


    private val settingsService: ApplicationSettingsService by di()

    private val settings = settingsService.settings


    override val root = hbox {
        setBorder(Color.DARKGRAY)
        padding = Insets(8.0)

        vbox {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }

            label(messages["time.tracker.data.importer.user.name.label"]) {
                vboxConstraints {
                    marginBottom = 6.0
                }
            }

            textfield(settings.timeTrackerDataImporterUserName) {
                useMaxWidth = true
                minHeight = TextFieldHeight
                maxHeight = minHeight

                focusedProperty().addListener { _, _, newValue ->
                    if (newValue == false) { // update settings on focus los
                        settings.timeTrackerDataImporterUserName = this.text
                        settingsService.settingsUpdated()
                    }
                }

            }

            label(messages["time.tracker.data.importer.password.label"]) {
                vboxConstraints {
                    marginTop = 10.0
                    marginBottom = 6.0
                }
            }

            textfield(settings.timeTrackerDataImporterPassword) {
                useMaxWidth = true
                minHeight = TextFieldHeight
                maxHeight = minHeight

                focusedProperty().addListener { _, _, newValue ->
                    if (newValue == false) { // update settings on focus los
                        settings.timeTrackerDataImporterPassword = this.text
                        settingsService.settingsUpdated()
                    }
                }

            }
        }

        vbox {
            alignment = Pos.CENTER
            useMaxHeight = true

            button("", ImageView(Icons.UpdateIconPath)) {
                contentDisplay = ContentDisplay.GRAPHIC_ONLY

                minWidth = 70.0
                maxWidth = minWidth
                minHeight = minWidth
                maxHeight = minHeight

                action {
                    refreshTimeTrackerDataListener?.invoke()
                }

                vboxConstraints {
                    marginLeft = 12.0
                }
            }
        }
    }

}