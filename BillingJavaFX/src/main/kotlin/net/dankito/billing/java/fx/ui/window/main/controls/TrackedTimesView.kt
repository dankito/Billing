package net.dankito.billing.java.fx.ui.window.main.controls

import javafx.collections.FXCollections
import javafx.scene.layout.Priority
import net.dankito.billing.java.fx.ui.window.main.model.TrackedMonthViewModel
import net.dankito.billing.timetracker.model.TrackedMonth
import tornadofx.*


class TrackedTimesView: View() {

    private val items = FXCollections.observableArrayList<TrackedMonth>()

    private val itemModel = TrackedMonthViewModel()


    override val root = vbox {
        prefHeight = 200.0

        listview<TrackedMonth>(items) {
            cellFragment(TrackedMonthCellFragment::class)

            bindSelected(itemModel)

            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
        }
    }


    fun setItems(items: List<TrackedMonth>) {
        this.items.setAll(items)
    }

}