package net.dankito.billing.java.fx.ui.window.invoice.model

import javafx.beans.property.DoubleProperty
import net.dankito.billing.model.Address
import net.dankito.billing.model.Client
import net.dankito.billing.settings.ApplicationSettings
import tornadofx.*


class ApplicationSettingsViewModel(val settings: ApplicationSettings) : ViewModel() {

    val clientName = bind { settings.lastSelectedClient.observable(Client::name) }

    val clientAddressStreetName = bind { settings.lastSelectedClient.address.observable(Address::streetName) }

    val clientAddressStreetNumber = bind { settings.lastSelectedClient.address.observable(Address::streetNumber) }

    val clientAddressPostalCode = bind { settings.lastSelectedClient.address.observable(Address::postalCode) }

    val clientAddressCity = bind { settings.lastSelectedClient.address.observable(Address::city) }


    val invoiceItemHourlyWage = bind { settings.observable(ApplicationSettings::invoiceItemHourlyWage) } as DoubleProperty

    val invoiceItemDescription = bind { settings.observable(ApplicationSettings::invoiceItemDescription)}

    val valueAddedTax = bind { settings.observable(ApplicationSettings::valueAddedTax) } as DoubleProperty

}