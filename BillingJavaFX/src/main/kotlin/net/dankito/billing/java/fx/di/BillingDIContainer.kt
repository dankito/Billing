package net.dankito.billing.java.fx.di

import net.dankito.billing.InvoiceCreator
import net.dankito.billing.java.fx.ui.presenter.MainWindowPresenter
import net.dankito.billing.settings.ApplicationSettingsService
import net.dankito.billing.timetracker.ITimeTrackerDataImporter
import net.dankito.billing.timetracker.harvest.HarvestTimeTrackerDataImporter
import tornadofx.*
import kotlin.reflect.KClass


class BillingDIContainer : DIContainer {

    private val settingsService = ApplicationSettingsService()

    private val billCreator = InvoiceCreator()

    private val timeTrackerDataImporter = HarvestTimeTrackerDataImporter()

    private val mainWindowPresenter = MainWindowPresenter(billCreator, timeTrackerDataImporter, settingsService)


    override fun <T : Any> getInstance(type: KClass<T>): T {
        (getInstanceForClass(type) as? T)?.let {instance ->
            return instance
        }

        throw Exception("Could not find an instance for class $type")
    }

    private fun getInstanceForClass(type: KClass<out Any>): Any? {
        when (type) {
            ApplicationSettingsService::class -> return settingsService
            InvoiceCreator::class -> return billCreator
            ITimeTrackerDataImporter::class -> return timeTrackerDataImporter
            MainWindowPresenter::class -> return mainWindowPresenter
        }

        return null
    }

}