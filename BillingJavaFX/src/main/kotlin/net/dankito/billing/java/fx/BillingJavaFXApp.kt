package net.dankito.billing.java.fx

import javafx.application.Application
import javafx.stage.Stage
import net.dankito.billing.java.fx.di.BillingDIContainer
import net.dankito.billing.java.fx.localization.UTF8ResourceBundleControl
import net.dankito.billing.java.fx.ui.window.main.MainWindow
import tornadofx.*
import java.util.*


class BillingJavaFXApp : App(MainWindow::class) {


    override fun start(stage: Stage) {
        setupMessagesResources() // has to be done before creating / injecting first instances as some of them already rely on Messages

        FX.dicontainer = BillingDIContainer()

        super.start(stage)
    }


    private fun setupMessagesResources() {
        ResourceBundle.clearCache() // at this point default ResourceBundles are already created and cached. In order that ResourceBundle created below takes effect cache has to be clearedbefore
        FX.messages = ResourceBundle.getBundle("Billing_Messages", UTF8ResourceBundleControl())
    }


    @Throws(Exception::class)
    override fun stop() {
        super.stop()
        System.exit(0) // otherwise Window would be closed but application still running in background
    }

}



fun main(args: Array<String>) {
    Application.launch(BillingJavaFXApp::class.java, *args)
}