package net.dankito.billing.java.fx.ui.window.invoice.model

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import net.dankito.billing.model.Invoice
import net.dankito.billing.timetracker.model.TrackedMonth
import tornadofx.*
import java.time.LocalDate


class InvoiceViewModel(trackedMonth: TrackedMonth) : ViewModel() {

    val invoicingDate = SimpleObjectProperty<LocalDate>(trackedMonth.lastDayOfTrackedMonth)

    val invoiceNumber = SimpleStringProperty(Invoice.calculateInvoiceNumber(invoicingDate.value))

    val invoiceStartDate = SimpleObjectProperty<LocalDate>(
            trackedMonth.firstTrackedDay ?: trackedMonth.firstDayOfTrackedMonth)

    val invoiceEndDate = SimpleObjectProperty<LocalDate>(
            trackedMonth.lastTrackedDay ?: trackedMonth.lastDayOfTrackedMonth)

}