package net.dankito.billing.java.fx.ui.window.invoice

import javafx.util.converter.NumberStringConverter
import javafx.util.converter.PercentageStringConverter
import net.dankito.billing.java.fx.ui.presenter.MainWindowPresenter
import net.dankito.billing.java.fx.ui.window.invoice.model.ApplicationSettingsViewModel
import net.dankito.billing.java.fx.ui.window.invoice.model.InvoiceViewModel
import net.dankito.billing.model.Invoice
import net.dankito.billing.settings.ApplicationSettingsService
import net.dankito.billing.timetracker.model.TrackedMonth
import tornadofx.*


class CustomizeInvoiceWindow : View() {

    companion object {
        private const val ButtonWidth = 150.0

        private const val ButtonHeight = 40.0

        private const val ButtonBottomAnchor = 24.0
    }


    override val root = Form()

    val trackedMonth: TrackedMonth by param()


    private val presenter: MainWindowPresenter by di()

    private val settingsService: ApplicationSettingsService by di()

    private val settings = settingsService.settings

    private val settingsViewModel = ApplicationSettingsViewModel(settings)

    private val invoiceViewModel = InvoiceViewModel(trackedMonth)


    init {
        initForm()
    }

    private fun initForm() {
        with (root) {
            fieldset(messages["customize.invoice.form.client.section.title"]) {
                field(messages["customize.invoice.form.client.name"]) {
                    textfield(settingsViewModel.clientName)
                }

                field(messages["customize.invoice.form.client.address.street.name.and.number"]) {
                    textfield(settingsViewModel.clientAddressStreetName)

                    textfield(settingsViewModel.clientAddressStreetNumber) {
                        minWidth = 60.0
                        maxWidth = minWidth
                    }
                }

                field(messages["customize.invoice.form.client.address.postal.code.and.city"]) {
                    textfield(settingsViewModel.clientAddressPostalCode) {
                        minWidth = 60.0
                        maxWidth = minWidth
                    }

                    textfield(settingsViewModel.clientAddressCity)
                }
            }

            fieldset(messages["customize.invoice.form.invoice.settings"]) {
                field(messages["customize.invoice.form.invoicing.date"]) {
                    datepicker(invoiceViewModel.invoicingDate)
                }

                field(messages["customize.invoice.form.invoice.number"]) {
                    textfield(invoiceViewModel.invoiceNumber)
                }

                field(messages["customize.invoice.form.invoice.start.date"]) {
                    datepicker(invoiceViewModel.invoiceStartDate)
                }

                field(messages["customize.invoice.form.invoice.end.date"]) {
                    datepicker(invoiceViewModel.invoiceEndDate)
                }

                field(messages["customize.invoice.form.invoice.value.added.tax"]) {
                    textfield().bind(settingsViewModel.valueAddedTax, false, PercentageStringConverter())
                }
            }

            fieldset(messages["customize.invoice.form.invoice.items.settings"]) {
                field(messages["customize.invoice.form.invoice.item.description"]) {
                    textfield(settingsViewModel.invoiceItemDescription) {
                        this.promptText = messages["customize.invoice.form.invoice.item.description.prompt.text"]
                    }
                }

                field(messages["customize.invoice.form.invoice.item.hourly.wage"]) {
                    textfield().bind(settingsViewModel.invoiceItemHourlyWage, false, NumberStringConverter())
                }
            }

            anchorpane {
                useMaxHeight = true

                button(messages["customize.invoice.form.create.invoice"]) {
                    minHeight = ButtonHeight
                    maxHeight = minHeight
                    minWidth = ButtonWidth
                    maxWidth = minWidth

                    action {
                        createInvoice()
                    }

                    anchorpaneConstraints {
                        rightAnchor = 0.0
                        bottomAnchor = ButtonBottomAnchor
                    }
                }

                button(messages["customize.invoice.form.cancel"]) {
                    minHeight = ButtonHeight
                    maxHeight = minHeight
                    minWidth = ButtonWidth
                    maxWidth = minWidth

                    action {
                        closeWindow()
                    }

                    anchorpaneConstraints {
                        rightAnchor = ButtonWidth + 12.0
                        bottomAnchor = ButtonBottomAnchor
                    }
                }
            }
        }
    }

    private fun createInvoice() {
        applySettingsChanges()

        val invoice = Invoice(
                listOf(presenter.createInvoiceItem(trackedMonth, settings)),
                invoiceViewModel.invoicingDate.value,
                invoiceViewModel.invoiceNumber.value,
                settings.valueAddedTax,
                settings.lastSelectedClient,
                invoiceViewModel.invoiceStartDate.value,
                invoiceViewModel.invoiceEndDate.value
        )

        presenter.createAndShowInvoice(invoice)

        closeWindow()
    }

    private fun applySettingsChanges() {
        settingsViewModel.commit()

        settingsService.settingsUpdated()
    }

    private fun closeWindow() {
        this.close()
    }

}