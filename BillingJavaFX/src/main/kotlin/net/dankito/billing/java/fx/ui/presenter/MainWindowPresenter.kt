package net.dankito.billing.java.fx.ui.presenter

import net.dankito.billing.InvoiceCreator
import net.dankito.billing.model.CreateInvoiceJob
import net.dankito.billing.model.Invoice
import net.dankito.billing.model.InvoiceItem
import net.dankito.billing.settings.ApplicationSettings
import net.dankito.billing.settings.ApplicationSettingsService
import net.dankito.billing.timetracker.ITimeTrackerDataImporter
import net.dankito.billing.timetracker.model.TrackedMonth
import net.dankito.billing.timetracker.model.TrackedTimes
import java.awt.Desktop
import java.io.File
import kotlin.concurrent.thread


class MainWindowPresenter(private val invoiceCreator: InvoiceCreator,
                          private val dataImporter: ITimeTrackerDataImporter,
                          private val settingsService: ApplicationSettingsService) {

    fun retrieveTrackedTimesAsync(callback: (trackedTimes: TrackedTimes) -> Unit) {
        val credentials = settingsService.settings.getTimeTrackerDataImporterCredentials()
        dataImporter.retrieveTrackedTimesAsync(credentials, callback)
    }


    fun createAndShowInvoice(trackedMonth: TrackedMonth) {
        val invoiceOutputFile = File(settingsService.settings.lastSelectedInvoiceOutputFilePath)

        createInvoiceFile(trackedMonth, invoiceOutputFile)

        openFileInOsDefaultApplication(invoiceOutputFile)
    }

    fun createAndShowInvoice(invoice: Invoice) {
        val invoiceOutputFile = File(settingsService.settings.lastSelectedInvoiceOutputFilePath)

        createInvoiceFile(invoice, invoiceOutputFile)

        openFileInOsDefaultApplication(invoiceOutputFile)
    }

    fun createInvoiceFile(trackedMonth: TrackedMonth, invoiceOutputFile: File) {
        val invoice = createInvoice(trackedMonth)

        createInvoiceFile(invoice, invoiceOutputFile)
    }

    fun createInvoiceFile(invoice: Invoice, invoiceOutputFile: File) {
        val invoiceTemplateFile = File(settingsService.settings.lastSelectedInvoiceTemplateFilePath)

        invoiceCreator.create(CreateInvoiceJob(invoice, invoiceTemplateFile, invoiceOutputFile))
    }

    fun createInvoice(trackedMonth: TrackedMonth): Invoice {
        val settings = settingsService.settings
        val lastDayOfMonth = trackedMonth.lastDayOfTrackedMonth

        return Invoice(listOf(
                createInvoiceItem(trackedMonth, settings)),
                lastDayOfMonth,
                invoiceStartDate = trackedMonth.firstTrackedDay ?: trackedMonth.firstDayOfTrackedMonth,
                invoiceEndDate = trackedMonth.lastTrackedDay ?: lastDayOfMonth,
                client = settings.lastSelectedClient)
    }

    fun createInvoiceItem(trackedMonth: TrackedMonth, settings: ApplicationSettings) =
            InvoiceItem(settings.invoiceItemHourlyWage, trackedMonth.decimalHours, settings.invoiceItemDescription)

    fun openFileInOsDefaultApplication(file: File) {
        thread { // get off UI thread
            try {
                Desktop.getDesktop().open(file)
            } catch(ignored: Exception) { }
        }
    }

}