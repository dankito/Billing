package net.dankito.billing.java.fx.ui.window.main.controls

import javafx.geometry.Pos
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import net.dankito.billing.java.fx.ui.presenter.MainWindowPresenter
import net.dankito.billing.java.fx.ui.window.invoice.CustomizeInvoiceWindow
import net.dankito.billing.java.fx.ui.window.main.model.TrackedMonthViewModel
import net.dankito.billing.settings.ApplicationSettingsService
import net.dankito.billing.timetracker.model.TrackedMonth
import tornadofx.*


class TrackedMonthCellFragment : ListCellFragment<TrackedMonth>() {

    private val trackedMonthItem = TrackedMonthViewModel().bindTo(this)

    private val presenter: MainWindowPresenter by di()

    private val settingsService: ApplicationSettingsService by di()

    private val settings = settingsService.settings


    override val root = vbox {
        cellProperty.addListener { _, _, newValue -> // so that the graphic always has cell's width
            newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }
        }

        paddingAll = 4.0

        borderpane {
            minHeight = 30.0
            maxHeight = minHeight

            alignment = Pos.CENTER_LEFT

            left {
                label(trackedMonthItem.date) {
                    font = Font.font(font.family, FontWeight.BOLD, 15.0)

                    alignment = Pos.CENTER_LEFT
                    minHeight = this@borderpane.minHeight
                }
            }

            right {
                hbox {
                    alignment = Pos.CENTER_RIGHT
                    minHeight = this@borderpane.minHeight

                    val decimalHourFont = Font.font(Font.getDefault().family, FontPosture.ITALIC, 15.0)

                    label(trackedMonthItem.decimalHours) {
                        font = decimalHourFont
                    }

                    label(messages["tracked.times.hour.label"]) {
                        font = decimalHourFont

                        paddingLeft = 2.0
                    }
                }
            }
        }

        anchorpane {
            minHeight = 40.0
            maxHeight = minHeight
            paddingTop = 8.0

            hbox {

                button(messages["create.customized.invoice.button"]) {
                    setOnMouseClicked { customizeInvoice() }
                }

                button(messages["quick.create.invoice.button"]) {
                    isDisable = settings.invoiceItemHourlyWage <= 0 // only enable quick invoice creation if hourly wage is set

                    setOnMouseClicked { createInvoice() }

                    hboxConstraints {
                        marginLeft = 12.0
                    }
                }

                anchorpaneConstraints {
                    this.topAnchor = 0
                    this.rightAnchor = 0
                    this.bottomAnchor = 0
                }
            }
        }
    }

    private fun customizeInvoice() {
        find<CustomizeInvoiceWindow>(mapOf(CustomizeInvoiceWindow::trackedMonth to this.item)).openModal()
    }

    private fun createInvoice() {
        presenter.createAndShowInvoice(this.item)
    }

}