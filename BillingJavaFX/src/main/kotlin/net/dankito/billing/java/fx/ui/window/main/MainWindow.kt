package net.dankito.billing.java.fx.ui.window.main

import javafx.geometry.Insets
import javafx.scene.layout.Priority
import javafx.stage.FileChooser
import net.dankito.billing.java.fx.ui.model.SelectFileType
import net.dankito.billing.java.fx.ui.presenter.MainWindowPresenter
import net.dankito.billing.java.fx.ui.views.SelectFileView
import net.dankito.billing.java.fx.ui.window.main.controls.TimeTrackerDataImporterSettingsView
import net.dankito.billing.java.fx.ui.window.main.controls.TrackedTimesView
import net.dankito.billing.settings.ApplicationSettingsService
import tornadofx.*


class MainWindow : View(FX.messages["application.title"]) {


    private val presenter: MainWindowPresenter by di()

    private val settingsService: ApplicationSettingsService by di()


    private val trackedTimesView = TrackedTimesView()

    private var selectInvoiceTemplateFileView: SelectFileView by singleAssign()

    private var selectInvoiceOutputFileView: SelectFileView by singleAssign()


    init {
        setupSelectFileViews()

        if (settingsService.settings.areTimeTrackerDataImporterCredentialsSet) {
            retrieveAndShowTrackedTimes()
        }
    }


    override val root = vbox {
        prefWidth = 850.0
        prefHeight = 600.0

        val timeTrackerDataImporterSettingsView = TimeTrackerDataImporterSettingsView {
            retrieveAndShowTrackedTimes()
        }
        add(timeTrackerDataImporterSettingsView)

        timeTrackerDataImporterSettingsView.root.vboxConstraints {
            margin = Insets(8.0, 4.0, 0.0, 4.0)
        }

        label(messages["tracked.times.label"]) {
            padding = Insets(18.0, 0.0, 8.0, 4.0)
        }

        add(trackedTimesView)

        trackedTimesView.root.vboxConstraints {
            vGrow = Priority.ALWAYS
        }

        add(selectInvoiceTemplateFileView)

        selectInvoiceTemplateFileView.root.vboxConstraints {
            useMaxWidth = true
            margin = Insets(12.0, 4.0, 0.0, 4.0)
        }

        add(selectInvoiceOutputFileView)

        selectInvoiceOutputFileView.root.vboxConstraints {
            useMaxWidth = true
            margin = Insets(6.0, 4.0, 0.0, 4.0)
        }
    }


    private fun setupSelectFileViews() {
        val settings = settingsService.settings

        selectInvoiceTemplateFileView = SelectFileView(messages["invoice.template.file"],
                SelectFileType.SelectFile, settings.lastSelectedInvoiceTemplateFilePath,
                listOf(FileChooser.ExtensionFilter(messages["odt.extension.filter.description"], "*.odt"))) { path ->
            settings.lastSelectedInvoiceTemplateFilePath = path
            settingsService.settingsUpdated()
        }

        selectInvoiceOutputFileView = SelectFileView(messages["invoice.output.file"],
                SelectFileType.SaveFile, settings.lastSelectedInvoiceOutputFilePath,
                listOf(FileChooser.ExtensionFilter(messages["pdf.extension.filter.description"], "*.pdf"))) { path ->
            settings.lastSelectedInvoiceOutputFilePath = path
            settingsService.settingsUpdated()
        }
    }

    private fun retrieveAndShowTrackedTimes() {
        presenter.retrieveTrackedTimesAsync { trackedTimes ->
            run {
                trackedTimesView.setItems(trackedTimes.months)
            }
        }
    }

}