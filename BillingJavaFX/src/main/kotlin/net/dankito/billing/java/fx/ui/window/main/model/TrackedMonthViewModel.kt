package net.dankito.billing.java.fx.ui.window.main.model

import javafx.beans.property.SimpleStringProperty
import net.dankito.billing.timetracker.model.TrackedMonth
import tornadofx.*
import java.time.format.DateTimeFormatter


class TrackedMonthViewModel : ItemViewModel<TrackedMonth>() {

    companion object {
        private val dateFormatter = DateTimeFormatter.ofPattern("MMMM yyyy")
    }


    val date = bind { SimpleStringProperty(item?.month?.format(dateFormatter)) }

    val decimalHours = bind { SimpleStringProperty(item?.decimalHoursString) }

}