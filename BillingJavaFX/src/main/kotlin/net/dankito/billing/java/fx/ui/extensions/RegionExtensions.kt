package net.dankito.billing.java.fx.ui.extensions

import javafx.geometry.Insets
import javafx.scene.layout.*
import javafx.scene.paint.Color


fun Region.setBackgroundColor(color: Color, radii: CornerRadii = CornerRadii.EMPTY, insets: Insets = Insets.EMPTY) {
    this.background = Background(BackgroundFill(color, radii, insets))
}

fun Region.setBorder(color: Color, style: BorderStrokeStyle = BorderStrokeStyle.SOLID,
                     radii: CornerRadii = CornerRadii(8.0), width: BorderWidths = BorderWidths(2.0),
                     insets: Insets = Insets.EMPTY) {

    this.border = Border(BorderStroke(color, style, radii, width, insets))
}